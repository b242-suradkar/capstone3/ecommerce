import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

function Register() {
  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [isActive, setIsActive] = useState(false);

  console.log(email);
  console.log(password1);
  console.log(password2);

// register user function
function registerUser(e) {
    e.preventDefault();

      fetch(`http://localhost:4000/users/checkEmail`, {
          method: "POST",
          headers: {
              'Content-Type': 'application/json'
          },
          body: JSON.stringify({
              email: email,
              password: password1
          })
      })
      .then(res => res.json())
      .then(data => {

          console.log(data);

          if(data === true){

              Swal.fire({
                  title: 'Duplicate email found',
                  icon: 'error',
                  text: 'Please provide a different email.'   
              });

          } else {

              fetch(`http://localhost:4000/users/register`, {
                  method: "POST",
                  headers: {
                      'Content-Type': 'application/json'
                  },
                  body: JSON.stringify({
                      
                      email: email,
                      
                      password: password1
                  })
              })
              .then(res => res.json())
              .then(data => {

                  console.log(data);

                  if(data === true){

                      // Clear input fields
                      
                      setEmail('');
                      
                      setPassword1('');
                      setPassword2('');

                      Swal.fire({
                          title: 'Registration successful',
                          icon: 'success',
                          text: 'Welcome to Zuitt!'
                      });

                      // Allows us to redirect the user to the login page after registering for an account
                      navigate("/login");

                  } else {

                      Swal.fire({
                          title: 'Something wrong',
                          icon: 'error',
                          text: 'Please try again.'   
                      });

                  };

              })
          };

      })

  }
  useEffect(() => {

      // Validation to enable submit button when all fields are populated and both passwords match
      if((email !== '' &&  password1 !== '' && password2 !== '') && (password1 === password2)){
          setIsActive(true);
      } else {
          setIsActive(false);
      }

  }, [ email,  password1, password2]);


  return (
    ( user.id !== null ) ?
    <Navigate to="/products" />
    :
    
  <div class="container-fluid ">
    <div class = "text-center">
    <h1 class="m-3 p-3 ">Register </h1>
    </div>
    <div class="mt-5 col-sm-12 col-md-6 offset-md-3">
    <Form onSubmit={(e) => registerUser(e)}>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control 
          type="email" 
          placeholder="Enter email"
          value={email}
          onChange={e => setEmail(e.target.value)}
          required  
          />
       
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control 
          type="password" 
          placeholder="Password" 
          value={password1}
          onChange={e => setPassword1(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control 
          type="password" 
          placeholder=" Verify Password"
          value={password2}
          onChange={e => setPassword2(e.target.value)}
          required
           />
      </Form.Group>

     <div class = "text-center">
      { isActive ? 
      <Button variant="primary" type="submit" id="submitBtn">
            Submit
        </Button>
        :
        <Button variant="danger" type="submit" id="submitBtn" disabled>
              Submit
          </Button>
        }
      </div>
    </Form>
    </div>
    </div>
  );
}

export default Register;