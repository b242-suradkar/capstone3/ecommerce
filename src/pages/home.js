import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import ProductCard from '../components/ProductCard';
import Features from '../components/Features';

export default function Home(){
    const data = {
        title: "E Cart shopping",
        content: "Best product at best price",
        destination: "/products",
        label: "Buy now!"
    }

    return (
        <Fragment>
          <Banner data={data}/>
          <Highlights />
          <Features />
        </Fragment>
    )
}
