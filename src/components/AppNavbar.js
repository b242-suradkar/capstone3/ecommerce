import Container from 'react-bootstrap/Container';
import { Fragment, useState, useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext'; 
import NavDropdown from 'react-bootstrap/NavDropdown';



function AppNavbar() {
  const { user } = useContext(UserContext);
  return (
  
      <Navbar bg="light" variant="light" expand="lg" >
        <Container fluid > 
          <Navbar.Brand as={NavLink} to="/">E Cart</Navbar.Brand>
          <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav className="ml-auto my-2 my-lg-0" style={{ maxHeight: '100px' }} navbarScroll>
            <Nav.Link as={NavLink} to="/home" exact>Home</Nav.Link>
            <Nav.Link as={NavLink} to="/products" exact>Products</Nav.Link>
            {(user.id !== null) ?
                  <Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link>
                  :
                  <Fragment>
                    <Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
                    <Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>
                  </Fragment>
                }
          </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>

     
    
  );
}

export default AppNavbar;