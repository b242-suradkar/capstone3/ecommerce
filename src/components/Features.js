import Carousel from 'react-bootstrap/Carousel';

function Features() {
  return (
    <div class="container-fluid mt-5 mb-5">
          <div class = "text-center ">
    <Carousel variant="dark">
      <Carousel.Item interval={1000}>
      
        <img
          className="d-block w-100 "
          src="https://media.istockphoto.com/id/1440610451/photo/grey-colored-van-driving-fast-on-freeway-for-overnight-express-dilevery.jpg?b=1&s=170667a&w=0&k=20&c=dVA5KQShYD20JMIenLh0eYdTsHSjyvh2LFD7pFNZcq0="
          alt="First slide"
          class = "height: 50px width: 100vh"
          
        />
        
        <Carousel.Caption>
          <h3 class ="color:white">Fastest Dilevery</h3>
          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item interval={500} >
        <img
          className="d-block w-100 "
          src="https://media.istockphoto.com/id/1360733082/photo/quality-research-concept-with-thematic-icons-hand-holds-a-magnifying-glass.jpg?b=1&s=170667a&w=0&k=20&c=-Y32PJmlH_MNvVgH9GRx4UGO_5VMqC6TkEjhamwo0AA="
          alt="Second slide"
          class = "height: 50px width: 100vh"
        />
        <Carousel.Caption>
          <h3 >Best Quality</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100 "
          src="https://media.istockphoto.com/id/1353595066/photo/high-price-low-value-scale-business-concept.jpg?b=1&s=170667a&w=0&k=20&c=kocI6z0T_1gl507MMA3JYgBSKBF-wxZX5v6-AqkmflY="
          alt="Third slide"
          class = "height: 50px width: 100vh"
        />
        <Carousel.Caption>
          <h3>Lowest price</h3>
          <p>
            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
          </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
     </div>
        </div>
  );
}

export default Features;